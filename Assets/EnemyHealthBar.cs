﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{

    public Transform enemy;
    void Update()
    {
        Vector3 pos = new Vector3(0, 3f, 0) + enemy.position;
        transform.position = pos;
    }
}