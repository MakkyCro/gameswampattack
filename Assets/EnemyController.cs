﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    public Rigidbody2D rb;
    public Slider enemyHealth;
    public Slider healthSlider;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI coinsText;
    public TextMeshProUGUI powerUpText;
    public Animator animator;
    public GameObject bullet;
    public Sprite DefeatScreen;
    int health = 100;
    Color32 c;
    private Color originalColor;
    private bool underEffect = false;
    private Vector2 originalVelocity;
    private bool colided = false;

    private Color lightRed = new Color(1, .6f, .6f, 1);

    [SerializeField]
    private List<SpriteRenderer> bodyParts;

    float time;
    float spawnTime;

    IEnumerator flashColor(float delay, Color c)
    {
        foreach (var renderer in bodyParts)
        {
            renderer.color *= c;
        }

        yield return new WaitForSeconds(delay);

        foreach (var renderer in bodyParts)
        {
            renderer.color = originalColor;
        }
    }

    IEnumerator getStopped(float delay)
    {
        underEffect = true;
        rb.velocity = Vector2.zero;
        yield return new WaitForSeconds(delay);
        if (this.health > 0 && !colided)
            rb.velocity = originalVelocity;
        underEffect = false;
    }

    IEnumerator getPushedBack(float delay, float amount)
    {
        underEffect = true;
        animator.SetBool("attack", false);
        rb.velocity = Vector2.zero;
        transform.position += new Vector3(amount, 0, 0);
        yield return new WaitForSeconds(delay);
        if (this.health > 0)
            rb.velocity = originalVelocity;
        underEffect = false;
    }

    void Start()
    {
        originalColor = bodyParts[0].color;
        healthSlider = GameObject.FindGameObjectWithTag("PlayerHealthSlider").GetComponent<Slider>();
        healthText = GameObject.FindGameObjectWithTag("HealthText").GetComponent<TextMeshProUGUI>();
        coinsText = GameObject.FindGameObjectWithTag("PlayerCoinsText").GetComponent<TextMeshProUGUI>();
        powerUpText = GameManagment.powerUpText.GetComponent<TextMeshProUGUI>();

        if (MenuScript.bg_num == 1)
            health = 100;
        else if (MenuScript.bg_num == 2)
            health = 150;
        else if (MenuScript.bg_num == 3)
            health = 200;
        else if (MenuScript.bg_num == 4)
            health = 250;
        else if (MenuScript.bg_num == 5)
            health = 300;
        else if (MenuScript.bg_num == 6)
            health = 350;
        else if (MenuScript.bg_num == 7)
            health = 400;
        else if (MenuScript.bg_num == 8)
            health = 450;
        enemyHealth.maxValue = health;

        enemyHealth.value = health;
        //animator = GetComponent<Animator>();
        rb.velocity = transform.right * Random.Range(-0.4f, -0.7f);
        originalVelocity = rb.velocity;
        SetRandomTime();
        time = 0;
    }

    public void SetSortLayer(string sortingLayer)
    {
        foreach (SpriteRenderer renderer in bodyParts)
        {
            renderer.sortingLayerName = sortingLayer;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Contains("bullet"))
        {
            Destroy(col.gameObject);
            if (StatsControl.wep_selected == "wep_1")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 12;
                    StartCoroutine(flashColor(.1f, lightRed));
                    StartCoroutine(getStopped(.1f));
                }
                else
                {
                    health -= 18;
                    StartCoroutine(flashColor(.2f, Color.red));
                    StartCoroutine(getPushedBack(.1f, .1f));
                }
            }
            else if (StatsControl.wep_selected == "wep_2")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 14;
                    StartCoroutine(flashColor(.2f, lightRed));
                    StartCoroutine(getStopped(.1f));
                }
                else
                {
                    health -= 21;
                    StartCoroutine(flashColor(.2f, Color.red));
                    StartCoroutine(getPushedBack(.15f, .15f));
                }
            }
            else if (StatsControl.wep_selected == "wep_3")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 7;
                    StartCoroutine(flashColor(.05f, lightRed));
                    StartCoroutine(getStopped(.05f));
                }
                else
                {
                    health -= 10;
                    StartCoroutine(flashColor(.1f, Color.red));
                    StartCoroutine(getPushedBack(.05f, .05f));
                }
            }
            else if (StatsControl.wep_selected == "wep_4")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 12;
                    StartCoroutine(flashColor(.07f, lightRed));
                    StartCoroutine(getStopped(.07f));
                }
                else
                {
                    health -= 18;
                    StartCoroutine(flashColor(.1f, Color.red));
                    StartCoroutine(getPushedBack(.1f, .07f));
                }
            }
            else if (StatsControl.wep_selected == "wep_5")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 25;
                    StartCoroutine(flashColor(.1f, lightRed));
                    StartCoroutine(getStopped(.2f));
                }
                else
                {
                    health -= 35;
                    StartCoroutine(flashColor(.2f, Color.red));
                    StartCoroutine(getPushedBack(.2f, .2f));
                }
            }
            else if (StatsControl.wep_selected == "wep_6")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 15;
                    StartCoroutine(flashColor(.12f, lightRed));
                    StartCoroutine(getStopped(.12f));
                }
                else
                {
                    health -= 23;
                    StartCoroutine(flashColor(.15f, Color.red));
                    StartCoroutine(getPushedBack(.15f, .15f));
                }
            }
            else if (StatsControl.wep_selected == "wep_7")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 30;
                    StartCoroutine(flashColor(.2f, lightRed));
                    StartCoroutine(getStopped(.25f));
                }
                else
                {
                    health -= 45;
                    StartCoroutine(flashColor(.2f, Color.red));
                    StartCoroutine(getPushedBack(.25f, .25f));
                }
            }
            else if (StatsControl.wep_selected == "wep_8")
            {
                if (GameManagment.bullet_red_upgrade == false)
                {
                    health -= 20;
                    StartCoroutine(flashColor(.2f, lightRed));
                    StartCoroutine(getStopped(.1f));
                }
                else
                {
                    health -= 30;
                    StartCoroutine(flashColor(.2f, Color.red));
                    StartCoroutine(getPushedBack(.1f, .15f));
                }
            }

            if (health <= 0 && !animator.GetBool("death"))
            {
                gameObject.GetComponent<Collider2D>().enabled = false;
                rb.velocity = Vector3.zero;
                enemyHealth.gameObject.SetActive(false);
                animator.SetBool("death", true);
                PlayerData.gold += Mathf.RoundToInt(Random.Range(10.0f, 20.0f) * Mathf.Clamp(MenuScript.bg_num * 0.7f, 1, 5));
                coinsText.text = PlayerData.gold.ToString();

                int num = Random.Range(0, 14);
                if (num == 0)
                {
                    int dur = Random.Range(5, 10);
                    powerUp.duration = dur;
                    c = new Color32(255, 255, 255, 255);
                    powerUpText.GetComponent<TextMeshProUGUI>().color = c;
                    powerUpText.GetComponent<TextMeshProUGUI>().text = "Power Up: " + dur + " seconds";
                    powerUpText.gameObject.SetActive(true);
                    GameManagment.bullet_red_upgrade = true;
                }
                Destroy(gameObject, gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + 1f);
            }
        }

        if (col.gameObject.name.Contains("enemyAttackTrigger"))
        {
            colided = true;
            rb.velocity = Vector2.zero;
            animator.SetBool("attack", true);
        }
    }

    void SetRandomTime()
    {
        spawnTime = Random.Range(1.0f, 3.5f);
    }

    void FixedUpdate()
    {
        time += Time.deltaTime;
        if (time >= spawnTime && animator.GetBool("attack"))
        {
            Attack();
            SetRandomTime();
            time = 0;
        }
    }

    void Attack()
    {
        healthSlider.value -= 5;
        if (healthSlider.value <= 0 && !GameManagment.meMyselAndI.gameOverScreen.activeSelf)
        {
            healthSlider.value = 0;
            healthText.text = "0 / " + PlayerData.max_health;
            GameManagment.meMyselAndI.gameOverScreen.SetActive(true);
            GameManagment.meMyselAndI.gameOverScreen.GetComponentInChildren<Image>().sprite = DefeatScreen;
            Time.timeScale = 0f;
        }
        healthText.text = healthSlider.value + " / " + healthSlider.maxValue;
    }

    void Update()
    {
        enemyHealth.value = health;
        if(Input.GetKeyDown("space") && GameManagment.meMyselAndI.gameOverScreen.activeSelf)
        {
            Cursor.visible = true;
            Time.timeScale = 1f;
            GameManagment.meMyselAndI.gameOverScreen.SetActive(false);
            SceneManager.LoadScene("SwampAttack1");
        } else if (Input.GetKeyDown("escape") && GameManagment.meMyselAndI.gameOverScreen.activeSelf)
        {
            Cursor.visible = true;
            Time.timeScale = 1f;
            GameManagment.meMyselAndI.gameOverScreen.SetActive(false);
            SceneManager.LoadScene("MainMenu");
            SceneManager.UnloadSceneAsync("SwampAttack1");
        }
    }
}
