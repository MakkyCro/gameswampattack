﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.SceneManagement;
using System;

public class MenuScript : MonoBehaviour
{
    public GameObject panel_mainMenu;
    public GameObject panel_ChooseLVL;
    public GameObject panel_Options;
    public GameObject panel_Oigri;
    public GameObject panel_login;
    public GameObject panel_CandC;
    public TextMeshProUGUI cheatInfoText;
    public TMP_Dropdown cb_usernames;
    public TMP_InputField tb_login;
    public TMP_InputField tb_cheat;
    public Button btn_login;
    public Button btn_createNew;
    public Button btn_logout;
    public Button btn_submitCode;
    public Button btn_goBackcANDc;
    public static int bg_num = 1;
    public static bool immortalCheat = false;

    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    public static string usernameLogin = "";

    void Start()
    {
        DontDestroyOnLoad(GameObject.FindGameObjectWithTag("music"));
        if (usernameLogin == "")
        {
            btn_logout.interactable = false;
            cb_usernames.interactable = true;
            btn_login.interactable = true;
            btn_createNew.interactable = true;
            tb_login.enabled = true;
            panel_mainMenu.SetActive(false);
        }
        else
        {
            btn_logout.interactable = true;
            cb_usernames.interactable = false;
            btn_login.interactable = false;
            btn_createNew.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            panel_login.SetActive(false);
        }

        try
        {
            System.IO.Directory.CreateDirectory(pathDocuments + "\\SwampAttack");
            if (!File.Exists(pathDocuments + "\\SwampAttack\\usernames.txt"))
            {
                File.Create(pathDocuments + "\\SwampAttack\\usernames.txt");
            }
        }
        catch { }

        RefreshDropDown();
    }

    public void LVL1Pressed()
    {
        bg_num = 1;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL2Pressed()
    {
        bg_num = 2;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL3Pressed()
    {
        bg_num = 3;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL4Pressed()
    {
        bg_num = 4;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL5Pressed()
    {
        bg_num = 5;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL6Pressed()
    {
        bg_num = 6;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL7Pressed()
    {
        bg_num = 7;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void LVL8Pressed()
    {
        bg_num = 8;
        SceneManager.LoadScene("SwampAttack1");
    }
    public void Logout()
    {
        btn_logout.interactable = false;
        cb_usernames.interactable = true;
        btn_login.interactable = true;
        btn_createNew.interactable = true;
        tb_login.enabled = true;
        panel_mainMenu.SetActive(false);
        panel_ChooseLVL.SetActive(false);
        panel_Options.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_login.SetActive(true);
        usernameLogin = "";
        RefreshDropDown();
        tb_login.text = "";
    }

    bool Exists()
    {
        var usernames = File.ReadAllLines(pathDocuments + "\\SwampAttack\\usernames.txt");
        var lista = new List<string>(usernames);
        bool postoji = false;

        foreach (string username in lista)
        {
            if (tb_login.text == username)
            {
                postoji = true;
                break;
            }
            else { postoji = false; }
        }

        return postoji;
    }

    public void LoginPressed()
    {
        if (tb_login.text == "" && cb_usernames.options[cb_usernames.value].text != "")
        {
            tb_login.text = cb_usernames.options[cb_usernames.value].text;
        }
        if (tb_login.text != "" && Exists())
        {
            cb_usernames.interactable = false;
            btn_logout.interactable = true;
            btn_login.interactable = false;
            btn_createNew.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            panel_login.SetActive(false);
            usernameLogin = tb_login.text;
            PlayerData.username = usernameLogin;

            LoadData();
        }
    }

    void LoadData()
    {
        if (File.Exists(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt"))
        {
            var usernames = File.ReadAllLines(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt");
            var arr = new List<string>(usernames);
            PlayerData.username = arr[0];
            PlayerData.gold = int.Parse(arr[1]);
            PlayerData.max_health = int.Parse(arr[2]);
            PlayerData.LVL1 = bool.Parse(arr[3]);
            PlayerData.LVL2 = bool.Parse(arr[4]);
            PlayerData.LVL3 = bool.Parse(arr[5]);
            PlayerData.LVL4 = bool.Parse(arr[6]);
            PlayerData.LVL5 = bool.Parse(arr[7]);
            PlayerData.LVL6 = bool.Parse(arr[8]);
            PlayerData.LVL7 = bool.Parse(arr[9]);
            PlayerData.LVL8 = bool.Parse(arr[10]);
            PlayerData.gun1 = bool.Parse(arr[11]);
            PlayerData.gun2 = bool.Parse(arr[12]);
            PlayerData.gun3 = bool.Parse(arr[13]);
            PlayerData.gun4 = bool.Parse(arr[14]);
            PlayerData.gun5 = bool.Parse(arr[15]);
            PlayerData.gun6 = bool.Parse(arr[16]);
            PlayerData.gun7 = bool.Parse(arr[17]);
            PlayerData.gun8 = bool.Parse(arr[18]);
            PlayerData.ammo2 = int.Parse(arr[19]);
            PlayerData.ammo3 = int.Parse(arr[20]);
            PlayerData.ammo4 = int.Parse(arr[21]);
            PlayerData.ammo5 = int.Parse(arr[22]);
            PlayerData.ammo6 = int.Parse(arr[23]);
            PlayerData.ammo7 = int.Parse(arr[24]);
            PlayerData.ammo8 = int.Parse(arr[25]);
        }
    }

    public void MakeNewProfile()
    {
        if (!Exists() && tb_login.text != "")
        {
            File.AppendAllText(pathDocuments + "\\SwampAttack\\usernames.txt", tb_login.text + "\n");
            cb_usernames.interactable = false;
            btn_logout.interactable = true;
            btn_login.interactable = false;
            btn_createNew.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            usernameLogin = tb_login.text;
            panel_login.SetActive(false);
            PlayerData.username = usernameLogin;

            if (!File.Exists(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt"))
            {
                using (File.Create(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt")) { }
            }
            File.WriteAllText(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + 0 + "\n" + 100 + "\n" + true
                + "\n" + false + "\n" + false + "\n" + false + "\n" + false + "\n" + false + "\n" + false + "\n" + false + "\n" + true + "\n" + false + "\n" + false + "\n" + false
                + "\n" + false + "\n" + false + "\n" + false + "\n" + false + "\n" + 0 + "\n" + 0 + "\n" + 0 + "\n" + 0 + "\n" + 0 + "\n" + 0 + "\n" + 0);

            LoadData();
        }

    }

    public void CopyValueToTB_login()
    {
        tb_login.text = cb_usernames.options[cb_usernames.value].text;
    }

    void RefreshDropDown()
    {
        if (!File.Exists(pathDocuments + "\\SwampAttack\\usernames.txt"))
        {
            using (File.Create(pathDocuments + "\\SwampAttack\\usernames.txt")) { }
        }

        if (new FileInfo(pathDocuments + "\\SwampAttack\\usernames.txt").Length != 0)
        {
            cb_usernames.interactable = true;
            var usernames = File.ReadAllLines(pathDocuments + "\\SwampAttack\\usernames.txt");
            List<string> lista = new List<string>(usernames);
            cb_usernames.options.Clear();
            foreach (string t in lista)
            {
                cb_usernames.options.Add(new TMP_Dropdown.OptionData() { text = t });
            }
            cb_usernames.RefreshShownValue();
        }
        else { cb_usernames.interactable = false; }
    }

    public void SubmitCheatAndCode()
    {
        if(tb_cheat.text == "give me ammo")
        {
            cheatInfoText.text = "\"" + tb_cheat.text + "\"" + " is activated. Shoot all you want.";
            PlayerData.ammo2 = 9999999;
            PlayerData.ammo3 = 9999999;
            PlayerData.ammo4 = 9999999;
            PlayerData.ammo5 = 9999999;
            PlayerData.ammo6 = 9999999;
            PlayerData.ammo7 = 9999999;
            PlayerData.ammo8 = 9999999;
            SaveChanges();
        }
        else if (tb_cheat.text == "make me immortal")
        {
            cheatInfoText.text = "\"" + tb_cheat.text + "\"" + " is activated. You are now almost indestructible.";
            PlayerData.max_health = 9999999;
            immortalCheat = true;
            SaveChanges();
        }
        else if (tb_cheat.text == "make it rain")
        {
            cheatInfoText.text = "\"" + tb_cheat.text + "\"" + " is activated. You are now rich.";
            PlayerData.gold = 9999999;
            SaveChanges();
        }
        else if (tb_cheat.text == "unlock everything")
        {
            cheatInfoText.text = "\"" + tb_cheat.text + "\"" + " is activated. Every weapon and level is now unlocked.";
            PlayerData.gun1 = true;
            PlayerData.gun2 = true;
            PlayerData.gun3 = true;
            PlayerData.gun4 = true;
            PlayerData.gun5 = true;
            PlayerData.gun6 = true;
            PlayerData.gun7 = true;
            PlayerData.gun8 = true;
            PlayerData.LVL1 = true;
            PlayerData.LVL2 = true;
            PlayerData.LVL3 = true;
            PlayerData.LVL4 = true;
            PlayerData.LVL5 = true;
            PlayerData.LVL6 = true;
            PlayerData.LVL7 = true;
            PlayerData.LVL8 = true;
            SaveChanges();
        }
        else
        {
            cheatInfoText.text = "\"" + tb_cheat.text + "\"" + " is not valid code.";
        }
        btn_goBackcANDc.interactable = false;
        btn_submitCode.interactable = false;

        StartCoroutine(hideTextCheat());
    }

    void SaveChanges()
    {
        File.WriteAllText(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.gold + "\n" + PlayerData.max_health + "\n" + PlayerData.LVL1
    + "\n" + PlayerData.LVL2 + "\n" + PlayerData.LVL3 + "\n" + PlayerData.LVL4 + "\n" + PlayerData.LVL5 + "\n" + PlayerData.LVL6
    + "\n" + PlayerData.LVL7 + "\n" + PlayerData.LVL8 + "\n" + PlayerData.gun1 + "\n" + PlayerData.gun2 + "\n" + PlayerData.gun3 + "\n" + PlayerData.gun4
    + "\n" + PlayerData.gun5 + "\n" + PlayerData.gun6 + "\n" + PlayerData.gun7 + "\n" + PlayerData.gun8 + "\n" + PlayerData.ammo2
    + "\n" + PlayerData.ammo3 + "\n" + PlayerData.ammo4 + "\n" + PlayerData.ammo5 + "\n" + PlayerData.ammo6 + "\n" + PlayerData.ammo7 + "\n" + PlayerData.ammo8);
    }

    IEnumerator hideTextCheat()
    {
        yield return new WaitForSeconds(5);
        cheatInfoText.text = "";
        btn_goBackcANDc.interactable = true;
        btn_submitCode.interactable = true;
    }


    public void AboutPressed()
    {
        panel_mainMenu.SetActive(false);
        panel_ChooseLVL.SetActive(false);
        panel_Options.SetActive(false);
        panel_Oigri.SetActive(true);
        panel_CandC.SetActive(false);
    }

    public void CheatsAndCodesPressed()
    {
        panel_mainMenu.SetActive(false);
        panel_ChooseLVL.SetActive(false);
        panel_Options.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_CandC.SetActive(true);
    }

    public void OptionsPressed()
    {
        panel_mainMenu.SetActive(false);
        panel_ChooseLVL.SetActive(false);
        panel_Options.SetActive(true);
        panel_Oigri.SetActive(false);
        panel_CandC.SetActive(false);
    }

    public void ChooseLVLPressed()
    {
        panel_mainMenu.SetActive(false);
        panel_ChooseLVL.SetActive(true);
        panel_Options.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_CandC.SetActive(false);

        var usernames = File.ReadAllLines(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt");
        var arr = new List<string>(usernames);

        foreach (Button btn in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn.name.Contains("LVL"))
            {
                int btn_num = int.Parse(btn.name.Split('_')[1]);
                if (bool.Parse(arr[2 + btn_num]) == true)
                {
                    btn.interactable = true;
                }
                else
                {
                    btn.interactable = false;
                }
            }
        }
    }

    public void ExitPressed()
    {
        Debug.Log("Exit pressed.");
        Application.Quit();
    }

    public void backPressed()
    {
        panel_mainMenu.SetActive(true);
        panel_ChooseLVL.SetActive(false);
        panel_Options.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_login.SetActive(false);
        panel_CandC.SetActive(false);
    }
}
