﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class powerUp : MonoBehaviour
{
    float time;
    float oneSecTime;
    public static int duration;
    Color32 c;
    void Start()
    {
        oneSecTime = 1;
        time = 0;
    }

    void FixedUpdate()
    {
        if(duration >= 0)
        {
            time += Time.deltaTime;
            if (time >= oneSecTime)
            {
                Debug.Log("sec left:" + duration.ToString());
                gameObject.GetComponent<TextMeshProUGUI>().text = "Power Up: " + duration + " seconds";
                duration -= 1;
                time = 0;
                if (duration < 3)
                {
                    c = new Color32(205, 0, 0, 255);
                    gameObject.GetComponent<TextMeshProUGUI>().color = c;
                }

                if (duration < 0)
                {
                    removePowerUp();
                    duration = 0;
                    gameObject.SetActive(false);
                }
            }
        }
    }
    void removePowerUp()
    {
        GameManagment.bullet_red_upgrade = false;
    }
}
