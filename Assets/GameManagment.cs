﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class GameManagment : MonoBehaviour
{
    public GameObject enemy;
    public GameObject bg;
    public static GameObject powerUpText;
    public static GameManagment meMyselAndI;

    public Transform firePoint1;
    public Transform firePoint2;
    public Transform firePoint3;
    public Transform firePoint4;
    public Transform firePoint5;
    public Transform firePoint6;
    public Transform firePoint7;
    public Transform firePoint8;

    public GameObject bullet_reg;
    public GameObject bullet_red;
    public GameObject PauseMenu;
    public GameObject crosshair;
    public Button btn_skipToNextWave;
    public GameObject gameOverScreen;

    public static Image gameOverImage;

    public Sprite bg1;
    public Sprite bg2;
    public Sprite bg3;
    public Sprite bg4;
    public Sprite bg5;
    public Sprite bg6;
    public Sprite bg7;
    public Sprite bg8;
    public Sprite victoryScreen;
    public Sprite defeatScreen;

    public float waveTimer = 30f;
    public int currentLevel = 1;
    private int currentWave = 0;
    private float timeTillWave = 0f;

    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI nextWaveText;
    public bool coolDown = false;
    public static bool bullet_red_upgrade = false;
    private Vector3 mousePosition;
    private AudioSource shootingSound;
    public AudioClip pistolSound;
    public AudioClip shotgunSound;
    public AudioClip sniperSound;

    float time;
    float spawnTime;
    int enemyCounter = 0;
    int lastSpawn;
    private bool waveEngaged;
    private bool onlyOnceGuard;
    private bool toMainMenu;

    void Awake()
    {
        GameManagment.meMyselAndI = this;
        powerUpText = GameObject.FindGameObjectWithTag("FuckMeUp");
        
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && !Input.GetKey(KeyCode.LeftControl) && !PauseMenu.activeSelf)
        {
            Shoot();
        }

        if (PauseMenu.activeSelf == false && Input.GetKeyDown("escape") && gameOverScreen.activeSelf == false)
        {
            crosshair.SetActive(false);
            Time.timeScale = 0f;
            PauseMenu.SetActive(true);
            Cursor.visible = true;
        }
        else if (PauseMenu.activeSelf == true && Input.GetKeyDown("escape") && gameOverScreen.activeSelf == false)
        {
            Time.timeScale = 1f;
            PauseMenu.SetActive(false);
            Cursor.visible = false;
            crosshair.SetActive(true);
        }
        else if (Input.GetKeyDown("escape") && gameOverScreen.activeSelf == true)
        {
            Cursor.visible = true;
            Time.timeScale = 1f;
            gameOverScreen.SetActive(false);
            MainMenuPressed();
        }
        else if (Input.GetKeyDown(KeyCode.Space) && gameOverScreen.activeSelf == true)
        {
            Time.timeScale = 1f;
            gameOverScreen.SetActive(false);
            Debug.Log(currentLevel + "");
            if(!toMainMenu)
            SceneManager.LoadScene("SwampAttack1");
            else
            {
                SceneManager.LoadScene("MainMenu");
                SceneManager.UnloadSceneAsync("SwampAttack1");
            }
        }

        if (Input.GetKey(KeyCode.LeftControl) && !PauseMenu.activeSelf)
        {
            foreach (Button btn in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn.name.Contains("wep") || btn.name.Contains("Upgrade"))
                {
                    btn.interactable = true;
                }
            }

            Cursor.visible = true;
            crosshair.SetActive(false);
        }
        if (!Input.GetKey(KeyCode.LeftControl) && !PauseMenu.activeSelf)
        {
            foreach (Button btn in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn.name.Contains("wep") || btn.name.Contains("Upgrade"))
                {
                    btn.interactable = false;
                }
            }

            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, getOffGunPointOffset(), 0);
            crosshair.transform.position = Vector2.Lerp(crosshair.transform.position, mousePosition, 100f);
            Cursor.visible = false;
            crosshair.SetActive(true);
        }

        if (timeTillWave <= 0 && !waveEngaged && currentWave < currentLevel * 2)
        {
            timeTillWave = waveTimer;
            waveEngaged = true;
            enemyCounter = 0;
            currentWave++;
        }
        else if (timeTillWave > 0 && GameObject.FindGameObjectsWithTag("Enemy").Length == 0 && currentWave < currentLevel * 2 && !waveEngaged)
        {
            btn_skipToNextWave.gameObject.SetActive(true);
        }
        else if (GameObject.FindGameObjectsWithTag("Enemy").Length != 0 && currentWave < currentLevel * 2 && !waveEngaged)
        {
            btn_skipToNextWave.gameObject.SetActive(false);
        }
        else if (currentWave == currentLevel * 2 && GameObject.FindGameObjectsWithTag("Enemy").Length == 0 && !waveEngaged)
        {
            if (MenuScript.bg_num < 8 && !gameOverScreen.activeSelf && !onlyOnceGuard)
            {
                timeTillWave = 0;
                nextWaveText.text = "Next wave in: 0 sec";
                if (currentLevel == 1)
                    PlayerData.LVL2 = true;
                else if (currentLevel == 2)
                    PlayerData.LVL3 = true;
                else if (currentLevel == 3)
                    PlayerData.LVL4 = true;
                else if (currentLevel == 4)
                    PlayerData.LVL5 = true;
                else if (currentLevel == 5)
                    PlayerData.LVL6 = true;
                else if (currentLevel == 6)
                    PlayerData.LVL7 = true;
                else if (currentLevel == 7)
                    PlayerData.LVL8 = true;
                onlyOnceGuard = true;
                MenuScript.bg_num++;
                Victory();
                Time.timeScale = 0f;
                // Application.LoadLevel("SwampAttack1");
            }
            else if(currentLevel == 8 && !gameOverScreen.activeSelf && !onlyOnceGuard)
            {
                toMainMenu = true;
                Victory();
            }
        }
        if (!waveEngaged && currentWave != currentLevel * 2)
        {
            timeTillWave -= Time.deltaTime;
            nextWaveText.text = "Next wave in: " + Mathf.RoundToInt(timeTillWave) + " sec";
        }
    }

    void Victory()
    {
        gameOverScreen.SetActive(true);
        gameOverScreen.GetComponentInChildren<Image>().sprite = victoryScreen;
    }

    float getOffGunPointOffset()
    {
        if (StatsControl.wep_selected == "wep_1")
        {
            return firePoint1.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_2")
        {
            return firePoint2.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_3")
        {
            return firePoint3.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_4")
        {
            return firePoint4.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_5")
        {
            return firePoint5.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_6")
        {
            return firePoint6.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_7")
        {
            return firePoint7.localPosition.y;
        }
        else if (StatsControl.wep_selected == "wep_8")
        {
            return firePoint8.localPosition.y;
        }
        return 0f;
    }

    void Start()
    {
        shootingSound = GetComponent<AudioSource>();
        nextWaveText.text = "Next wave in: 0 sec";
        if (MenuScript.bg_num == 1)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg1;
        }
        else if (MenuScript.bg_num == 2)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg2;
        }
        else if (MenuScript.bg_num == 3)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg3;
        }
        else if (MenuScript.bg_num == 4)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg4;
        }
        else if (MenuScript.bg_num == 5)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg5;
        }
        else if (MenuScript.bg_num == 6)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg6;
        }
        else if (MenuScript.bg_num == 7)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg7;
        }
        else if (MenuScript.bg_num == 8)
        {
            bg.GetComponent<SpriteRenderer>().sprite = bg8;
        }

        currentLevel = MenuScript.bg_num;
        SetRandomTime();
        time = 0;
        Cursor.visible = false;
    }

    void SetRandomTime()
    {
        spawnTime = Random.Range(1.0f, 3.5f);
    }

    void FixedUpdate()
    {
        if (waveEngaged)
        {
            time += Time.deltaTime;
            if (time >= spawnTime && enemyCounter < currentLevel + 1 + currentWave)
            {
                Spawn();
                SetRandomTime();
                time = 0;
                enemyCounter++;
            }
            else if (enemyCounter >= currentLevel + 1 + currentWave)
            {
                waveEngaged = false;
            }
        }
    }

    void Spawn()
    {
        int a = Random.Range(0, 4);
        while (a == lastSpawn)
            a = Random.Range(0, 4);
        lastSpawn = a;
        Vector3 position = new Vector3(10, -5f + a / 10f, 0);
        GameObject Prefab = Instantiate(enemy, position, Quaternion.identity);
        EnemyController e = Prefab.GetComponent<EnemyController>();
        switch (a + 1)
        {
            case 1:
                e.SetSortLayer("EnemyLayer5");
                break;
            case 2:
                e.SetSortLayer("EnemyLayer4");
                break;
            case 3:
                e.SetSortLayer("EnemyLayer3");
                break;
            case 4:
                e.SetSortLayer("EnemyLayer2");
                break;
            case 5:
                e.SetSortLayer("EnemyLayer1");
                break;
        }
    }

    public void Shoot()
    {
        if (StatsControl.wep_selected == "wep_1" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.5f));
        }
        else if (StatsControl.wep_selected == "wep_2" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.45f));
        }
        else if (StatsControl.wep_selected == "wep_3" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.04f));
        }
        else if (StatsControl.wep_selected == "wep_4" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.045f));
        }
        else if (StatsControl.wep_selected == "wep_5" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = shotgunSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.4f));
        }
        else if (StatsControl.wep_selected == "wep_6" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.03f));
        }
        else if (StatsControl.wep_selected == "wep_7" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = sniperSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.3f));
        }
        else if (StatsControl.wep_selected == "wep_8" && coolDown == false && ammoText.text != "RELOAD")
        {
            shootingSound.clip = pistolSound;
            coolDown = true;
            StartCoroutine(cooldownTimer(0.1f));
        }

    }

    IEnumerator cooldownTimer(float time)
    {
        yield return new WaitForSeconds(time);
        shootingSound.Play();
        coolDown = false;

        if (StatsControl.wep_selected == "wep_1" && StatsControl.mag_gun1 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint1.position, firePoint1.rotation);
            else
                Instantiate(bullet_red, firePoint1.position, firePoint1.rotation);
            StatsControl.mag_gun1 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_2" && StatsControl.mag_gun2 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint2.position, firePoint2.rotation);
            else
                Instantiate(bullet_red, firePoint2.position, firePoint2.rotation);

            // PlayerData.ammo2 -= 1;
            StatsControl.mag_gun2 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_3" && StatsControl.mag_gun3 > 0)
        {

            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint3.position, firePoint3.rotation);
            else
                Instantiate(bullet_red, firePoint3.position, firePoint3.rotation);


            // PlayerData.ammo3 -= 1;
            StatsControl.mag_gun3 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_4" && StatsControl.mag_gun4 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint4.position, firePoint4.rotation);
            else
                Instantiate(bullet_red, firePoint4.position, firePoint4.rotation);

            // PlayerData.ammo4 -= 1;
            StatsControl.mag_gun4 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_5" && StatsControl.mag_gun5 > 0)
        {
            for (int i = 0; i < 3; i++)
            {
                var offset = new Vector3(0, Random.Range(-0.15f, 0.15f), 0);
                if (bullet_red_upgrade == false)
                    Instantiate(bullet_reg, firePoint5.position + offset, firePoint5.rotation);
                else
                    Instantiate(bullet_red, firePoint5.position + offset, firePoint5.rotation);
            }

            // PlayerData.ammo5 -= 1;
            StatsControl.mag_gun5 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_6" && StatsControl.mag_gun6 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint6.position, firePoint6.rotation);
            else
                Instantiate(bullet_red, firePoint6.position, firePoint6.rotation);

            // PlayerData.ammo6 -= 1;
            StatsControl.mag_gun6 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_7" && StatsControl.mag_gun7 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint7.position, firePoint7.rotation);
            else
                Instantiate(bullet_red, firePoint7.position, firePoint7.rotation);

            // PlayerData.ammo7 -= 1;
            StatsControl.mag_gun7 -= 1;
        }
        else if (StatsControl.wep_selected == "wep_8" && StatsControl.mag_gun8 > 0)
        {
            if (bullet_red_upgrade == false)
                Instantiate(bullet_reg, firePoint8.position, firePoint8.rotation);
            else
                Instantiate(bullet_red, firePoint8.position, firePoint8.rotation);

            // PlayerData.ammo8 -= 1;
            StatsControl.mag_gun8 -= 1;
        }

        try
        {
            StatsControl sn = gameObject.GetComponent<StatsControl>();
            sn.RefreshStatsAndSave();
        }
        catch { }
    }

    public void SkipPressed()
    {
        timeTillWave = 0;
        nextWaveText.text = "Next wave in: 0 sec";
        btn_skipToNextWave.gameObject.SetActive(false);
    }

    public void ResumePressed()
    {
        Time.timeScale = 1f;
        PauseMenu.SetActive(false);
        Cursor.visible = false;
    }

    public void RestartPressed()
    {
        SceneManager.LoadScene("SwampAttack1");
        // Application.LoadLevel("SwampAttack1");
        Cursor.visible = false;
    }

    public void MainMenuPressed()
    {
        Cursor.visible = true;
        SceneManager.LoadScene("MainMenu");
        SceneManager.UnloadSceneAsync("SwampAttack1");
        // Application.LoadLevel("MainMenu");
    }

    public void QuitPressed()
    {
        Application.Quit();
    }
}
