﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using System;

public class PlayerData : MonoBehaviour
{
    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    public static string username = "default";

    public static int gold;
    public static int max_health;

    public static bool LVL1;
    public static bool LVL2;
    public static bool LVL3;
    public static bool LVL4;
    public static bool LVL5;
    public static bool LVL6;
    public static bool LVL7;
    public static bool LVL8;

    public static bool gun1;
    public static bool gun2;
    public static bool gun3;
    public static bool gun4;
    public static bool gun5;
    public static bool gun6;
    public static bool gun7;
    public static bool gun8;

    public static int ammo2;
    public static int ammo3;
    public static int ammo4;
    public static int ammo5;
    public static int ammo6;
    public static int ammo7;
    public static int ammo8;

}
