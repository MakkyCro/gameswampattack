﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarCanvasHelpScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Canvas c = gameObject.GetComponent<Canvas>();
        // c.renderMode = RenderMode.ScreenSpaceCamera;
        c.worldCamera = Camera.main;
        c.sortingLayerName = "hud";
    }
}
