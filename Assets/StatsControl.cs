﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.IO;
using System;

public class StatsControl : MonoBehaviour
{
    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    public GameObject wep1;
    public GameObject wep2;
    public GameObject wep3;
    public GameObject wep4;
    public GameObject wep5;
    public GameObject wep6;
    public GameObject wep7;
    public GameObject wep8;
    public GameObject bone1;
    public GameObject bone2;
    public GameObject bone3;
    public GameObject bone4;
    public GameObject bone5;
    public GameObject bone6;
    public GameObject bone7;
    public GameObject bone8;
    public GameObject panel_weaponStats;
    public GameObject upgrade1;
    public GameObject upgrade2;
    public GameObject upgrade3;
    public GameObject upgrade4;
    public GameObject upgrade5;

    public Button btn_ammo;
    public Button btn_upgrade;

    public Slider health;
    public Slider slider_power;
    public Slider slider_speed;

    public TextMeshProUGUI healthText;
    public TextMeshProUGUI coinsText;
    public TextMeshProUGUI ammoText;

    int[] wep_price = { 100, 200, 300, 400, 500, 600, 1000 };
    int[] upgrade_price = { 50, 75, 125, 200, 500 };

    public static string wep_selected = "wep_1";

    public static int mag_gun1 = 0;
    public static int mag_gun2 = 0;
    public static int mag_gun3 = 0;
    public static int mag_gun4 = 0;
    public static int mag_gun5 = 0;
    public static int mag_gun6 = 0;
    public static int mag_gun7 = 0;
    public static int mag_gun8 = 0;

    private static bool reloading = false;

    // Start is called before the first frame update
    void Start()
    {
        coinsText.text = PlayerData.gold.ToString();

        health.maxValue = PlayerData.max_health;
        health.value = health.maxValue;
        healthText.text = health.value + " / " + PlayerData.max_health;
        UpdateWeaponsLayout();
        GameObject btn = GameObject.Find("wep_1");
        mag_gun1 = 10;
        btn.GetComponentInChildren<TextMeshProUGUI>().text = "Selected";

        if (PlayerData.max_health == 100)
        {
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[0].ToString();
        }
        else if (PlayerData.max_health == 150)
        {
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[1].ToString();
            upgrade1.SetActive(true);
        }
        else if (PlayerData.max_health == 200)
        {
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[2].ToString();
            upgrade1.SetActive(true);
            upgrade2.SetActive(true);
        }
        else if (PlayerData.max_health == 250)
        {
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[3].ToString();
            upgrade1.SetActive(true);
            upgrade2.SetActive(true);
            upgrade3.SetActive(true);
        }
        else if (PlayerData.max_health == 300)
        {
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[4].ToString();
            upgrade1.SetActive(true);
            upgrade2.SetActive(true);
            upgrade3.SetActive(true);
            upgrade4.SetActive(true);
        }
        else if (PlayerData.max_health >= 350)
        {
            btn_upgrade.interactable = false;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = "MAX";
            upgrade1.SetActive(true);
            upgrade2.SetActive(true);
            upgrade3.SetActive(true);
            upgrade4.SetActive(true);
            upgrade5.SetActive(true);
        }
    }

    void UpdateWeaponsLayout()
    {
        var usernames = File.ReadAllLines(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt");
        var arr = new List<string>(usernames);

        foreach (Button btn in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn.name.Contains("wep") && !btn.name.Contains("ammo"))
            {
                int btn_num = int.Parse(btn.name.Split('_')[1]);

                if (bool.Parse(arr[10 + btn_num]) == true || btn_num == 1)
                {
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "Select";
                    Destroy(GameObject.Find("coin_" + btn_num));
                }
                else
                {
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = wep_price[btn_num - 2].ToString();
                }
            }
        }
    }

    public void OnHover(Button btn)
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            panel_weaponStats.SetActive(true);
        }

        if (btn.name.Contains("1"))
        {
            slider_power.value = 17;
            slider_speed.value = 20;
        }
        else if (btn.name.Contains("2"))
        {
            slider_power.value = 20;
            slider_speed.value = 22;
        }
        else if (btn.name.Contains("3"))
        {
            slider_power.value = 22;
            slider_speed.value = 50;
        }
        else if (btn.name.Contains("4"))
        {
            slider_power.value = 30;
            slider_speed.value = 40;
        }
        else if (btn.name.Contains("5"))
        {
            slider_power.value = 50;
            slider_speed.value = 20;
        }
        else if (btn.name.Contains("6"))
        {
            slider_power.value = 40;
            slider_speed.value = 55;
        }
        else if (btn.name.Contains("7"))
        {
            slider_power.value = 75;
            slider_speed.value = 30;
        }
        else if (btn.name.Contains("8"))
        {
            slider_power.value = 60;
            slider_speed.value = 80;
        }
    }

    public void OnHoverExit()
    {
        panel_weaponStats.SetActive(false);
    }

    public void OnClicked()
    {
        GameObject btn = GameObject.Find(EventSystem.current.currentSelectedGameObject.name);
        int btn_num = int.Parse(btn.name.Split('_')[1]);

        if (btn.GetComponentInChildren<TextMeshProUGUI>().text != "Select" && btn.GetComponentInChildren<TextMeshProUGUI>().text != "Selected")
        {
            int price = int.Parse(btn.GetComponentInChildren<TextMeshProUGUI>().text);
            if (PlayerData.gold >= price)
            {
                PlayerData.gold -= price;
                Destroy(GameObject.Find("coin_" + btn_num));
                btn.GetComponentInChildren<TextMeshProUGUI>().text = "Select";

                if (btn_num == 2)
                {
                    PlayerData.gun2 = true;
                }
                else if (btn_num == 3)
                {
                    PlayerData.gun3 = true;
                }
                else if (btn_num == 4)
                {
                    PlayerData.gun4 = true;
                }
                else if (btn_num == 5)
                {
                    PlayerData.gun5 = true;
                }
                else if (btn_num == 6)
                {
                    PlayerData.gun6 = true;
                }
                else if (btn_num == 7)
                {
                    PlayerData.gun7 = true;
                }
                else if (btn_num == 8)
                {
                    PlayerData.gun8 = true;
                }
            }
        }
        else if (btn.GetComponentInChildren<TextMeshProUGUI>().text == "Select")
        {
            foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn1.GetComponentInChildren<TextMeshProUGUI>() != null)
                    if (btn1.GetComponentInChildren<TextMeshProUGUI>().text == "Selected")
                    {
                        btn1.GetComponentInChildren<TextMeshProUGUI>().text = "Select";
                        wep1.SetActive(false);
                        wep2.SetActive(false);
                        wep3.SetActive(false);
                        wep4.SetActive(false);
                        wep5.SetActive(false);
                        wep6.SetActive(false);
                        wep7.SetActive(false);
                        wep8.SetActive(false);
                        break;
                    }
            }
            btn.GetComponentInChildren<TextMeshProUGUI>().text = "Selected";
            wep_selected = btn.name;

            if (wep_selected == "wep_1")
            {
                wep1.SetActive(true);
                btn_ammo.interactable = false;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "0";
            }
            else if (wep_selected == "wep_2")
            {
                wep2.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "5";
            }
            else if (wep_selected == "wep_3")
            {
                wep3.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "10";
            }
            else if (wep_selected == "wep_4")
            {
                wep4.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "12";
            }
            else if (wep_selected == "wep_5")
            {
                wep5.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "15";
            }
            else if (wep_selected == "wep_6")
            {
                wep6.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "15";
            }
            else if (wep_selected == "wep_7")
            {
                wep7.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "17";
            }
            else if (wep_selected == "wep_8")
            {
                wep8.SetActive(true);
                btn_ammo.interactable = true;
                btn_ammo.GetComponentInChildren<TextMeshProUGUI>().text = "20";
            }
        }

        RefreshStatsAndSave();
    }

    void Update()
    {
        if (wep_selected == "wep_1")
        {
            if (mag_gun1 == 0 && !reloading)
                StartCoroutine(Reload("wep_1"));
            else
                ammoText.text = mag_gun1 + " / UNLIMITED";
        }
        else if (wep_selected == "wep_2")
        {
            if (mag_gun2 == 0 && PlayerData.ammo2 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_2"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun2 + " / " + (PlayerData.ammo2);
        }
        else if (wep_selected == "wep_3")
        {
            if (mag_gun3 == 0 && PlayerData.ammo3 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_3"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun3 + " / " + (PlayerData.ammo3);
        }
        else if (wep_selected == "wep_4")
        {
            if (mag_gun4 == 0 && PlayerData.ammo4 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_4"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun4 + " / " + (PlayerData.ammo4);
        }
        else if (wep_selected == "wep_5")
        {
            if (mag_gun5 == 0 && PlayerData.ammo5 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_5"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun5 + " / " + (PlayerData.ammo5);
        }
        else if (wep_selected == "wep_6")
        {
            if (mag_gun6 == 0 && PlayerData.ammo6 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_6"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun6 + " / " + (PlayerData.ammo6);
        }
        else if (wep_selected == "wep_7")
        {
            if (mag_gun7 == 0 && PlayerData.ammo7 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_7"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun7 + " / " + (PlayerData.ammo7);
        }
        else if (wep_selected == "wep_8")
        {
            if (mag_gun8 == 0 && PlayerData.ammo8 > 0 && !reloading)
            {
                ammoText.text = "RELOAD";
                StartCoroutine(Reload("wep_8"));
            }
            else
                ammoText.text = "AMMO: " + mag_gun8 + " / " + (PlayerData.ammo8);
        }
        FollowMouse();
    }

    IEnumerator Reload(string wep)
    {
        reloading = true;
        yield return new WaitForSeconds(1);
        if (wep_selected == "wep_1")
        {
            mag_gun1 = 10;
        }
        else if (wep_selected == "wep_2")
        {
            mag_gun2 = Mathf.Clamp(7, 0, PlayerData.ammo2);
            PlayerData.ammo2 -= mag_gun2;
        }
        else if (wep_selected == "wep_3")
        {
            mag_gun3 = Mathf.Clamp(30, 0, PlayerData.ammo3);
            PlayerData.ammo3 -= mag_gun3;
        }
        else if (wep_selected == "wep_4")
        {
            mag_gun4 = Mathf.Clamp(25, 0, PlayerData.ammo4);
            PlayerData.ammo4 -= mag_gun4;
        }
        else if (wep_selected == "wep_5")
        {
            mag_gun5 = Mathf.Clamp(7, 0, PlayerData.ammo5);
            PlayerData.ammo5 -= mag_gun5;
        }
        else if (wep_selected == "wep_6")
        {
            mag_gun6 = Mathf.Clamp(30, 0, PlayerData.ammo6);
            PlayerData.ammo6 -= mag_gun6;
        }
        else if (wep_selected == "wep_7")
        {
            mag_gun7 = Mathf.Clamp(10, 0, PlayerData.ammo7);
            PlayerData.ammo7 -= mag_gun7;
        }
        else if (wep_selected == "wep_8")
        {
            mag_gun8 = Mathf.Clamp(50, 0, PlayerData.ammo8);
            PlayerData.ammo8 -= mag_gun8;
        }
        reloading = false;
    }

    public void FollowMouse()
    {
        Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(bone1.transform.position);
        float AngleDeg = Mathf.Atan2(dir.y - bone1.transform.position.y, dir.x - bone1.transform.position.x) * Mathf.Rad2Deg;

        AngleDeg = Mathf.Clamp(AngleDeg, -20f, 20f);
        bone1.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone2.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone3.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone4.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone5.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone6.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone7.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        bone8.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);

    }

    public void AmmoPressed()
    {

        if (wep_selected == "wep_2" && PlayerData.gold >= 5)
        {
            PlayerData.gold -= 5;
            PlayerData.ammo2 += 21;
        }
        else if (wep_selected == "wep_3" && PlayerData.gold >= 10)
        {
            PlayerData.gold -= 10;
            PlayerData.ammo3 += 60;
        }
        else if (wep_selected == "wep_4" && PlayerData.gold >= 12)
        {
            PlayerData.gold -= 12;
            PlayerData.ammo4 += 50;
        }
        else if (wep_selected == "wep_5" && PlayerData.gold >= 15)
        {
            PlayerData.gold -= 15;
            PlayerData.ammo5 += 14;
        }
        else if (wep_selected == "wep_6" && PlayerData.gold >= 15)
        {
            PlayerData.gold -= 15;
            PlayerData.ammo6 += 60;
        }
        else if (wep_selected == "wep_7" && PlayerData.gold >= 17)
        {
            PlayerData.gold -= 17;
            PlayerData.ammo7 += 20;
        }
        else if (wep_selected == "wep_8" && PlayerData.gold >= 20)
        {
            PlayerData.gold -= 20;
            PlayerData.ammo8 += 100;
        }

        RefreshStatsAndSave();
    }

    public void UpgradePressed()
    {
        // int cur_max_health = PlayerData.max_health;
        // int price;
        // try
        // {
        //     price = int.Parse(btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text);
        // }
        // catch { Debug.Log("ERROR!"); }

        if (PlayerData.max_health == 100 && PlayerData.gold >= upgrade_price[0])
        {
            PlayerData.gold -= upgrade_price[0];
            PlayerData.max_health += 50;
            health.maxValue = PlayerData.max_health;
            health.value = PlayerData.max_health;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[1].ToString();
            upgrade1.SetActive(true);
        }
        else if (PlayerData.max_health == 150 && PlayerData.gold >= upgrade_price[1])
        {
            PlayerData.gold -= upgrade_price[1];
            PlayerData.max_health += 50;
            health.maxValue = PlayerData.max_health;
            health.value = PlayerData.max_health;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[2].ToString();
            upgrade2.SetActive(true);
        }
        else if (PlayerData.max_health == 200 && PlayerData.gold >= upgrade_price[2])
        {
            PlayerData.gold -= upgrade_price[2];
            PlayerData.max_health += 50;
            health.maxValue = PlayerData.max_health;
            health.value = PlayerData.max_health;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[3].ToString();
            upgrade3.SetActive(true);
        }
        else if (PlayerData.max_health == 250 && PlayerData.gold >= upgrade_price[3])
        {
            PlayerData.gold -= upgrade_price[3];
            PlayerData.max_health += 50;
            health.maxValue = PlayerData.max_health;
            health.value = PlayerData.max_health;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = upgrade_price[4].ToString();
        }
        else if (PlayerData.max_health == 300 && PlayerData.gold >= upgrade_price[4])
        {
            PlayerData.gold -= upgrade_price[4];
            PlayerData.max_health += 50;
            health.maxValue = PlayerData.max_health;
            health.value = PlayerData.max_health;
            btn_upgrade.interactable = false;
            btn_upgrade.GetComponentInChildren<TextMeshProUGUI>().text = "Max";
        }
        RefreshStatsAndSave();
    }

    public void RefreshStatsAndSave()
    {
        coinsText.text = PlayerData.gold.ToString();
        health.maxValue = PlayerData.max_health;
        healthText.text = health.value + " / " + health.maxValue;

        File.WriteAllText(pathDocuments + "\\SwampAttack\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.gold + "\n" + PlayerData.max_health + "\n" + PlayerData.LVL1
    + "\n" + PlayerData.LVL2 + "\n" + PlayerData.LVL3 + "\n" + PlayerData.LVL4 + "\n" + PlayerData.LVL5 + "\n" + PlayerData.LVL6
    + "\n" + PlayerData.LVL7 + "\n" + PlayerData.LVL8 + "\n" + PlayerData.gun1 + "\n" + PlayerData.gun2 + "\n" + PlayerData.gun3 + "\n" + PlayerData.gun4
    + "\n" + PlayerData.gun5 + "\n" + PlayerData.gun6 + "\n" + PlayerData.gun7 + "\n" + PlayerData.gun8 + "\n" + PlayerData.ammo2
    + "\n" + PlayerData.ammo3 + "\n" + PlayerData.ammo4 + "\n" + PlayerData.ammo5 + "\n" + PlayerData.ammo6 + "\n" + PlayerData.ammo7 + "\n" + PlayerData.ammo8);
    }


}
